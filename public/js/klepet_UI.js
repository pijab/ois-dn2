function spremeni(sporocilo) {
  return String(sporocilo).replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

function zamenjajSmejkote(sporocilo) {
  sporocilo = sporocilo.replace(';)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>');
  sporocilo = sporocilo.replace(':)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>');
  sporocilo = sporocilo.replace('(y)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>');
  sporocilo = sporocilo.replace(':*', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>');
  sporocilo = sporocilo.replace(':(', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>');

  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}
 
var besede = [];

function razdelipoPresledkih(sporocilo) {
  var besedeSporocila = [];
  besedeSporocila = sporocilo.split(" ");
  
  for (var i = 0; i < besede.length; i++) {
    for (var j = 0; j < besedeSporocila.length; j++) {
      var n = besedeSporocila[j].toLowerCase().indexOf(besede[i]);
      if (n >= 0) {
        var len = besede[i].length; 
        var len2 = besedeSporocila[j].length;
        
        var cenzura = "";
      
        for (var k = 0; k < len; k++)
          cenzura += "*";
        
        if (n === 0) {
          if ((n+len <= len2-1) && besedeSporocila[j][n+len].match(/[a-z]/i)) {
              continue;
          } else {
              besedeSporocila[j] = besedeSporocila[j].replace(besedeSporocila[j].substring(n, n+len), cenzura);
          }
        } else {
          if (besedeSporocila[j][n-1].match(/[a-z]/i)) {
            if ((n+len <= len2-1) && besedeSporocila[j][n+len].match(/[a-z]/i)) {
              continue;
            }
          } else {
              besedeSporocila[j] = besedeSporocila[j].replace(besedeSporocila[j].substring(n, n+len), cenzura);
          }
        }
      }
    }
  }
  return besedeSporocila.join(" ");
}

function preveriVulgarneBesede(sporocilo) {
  for (var i = 0; i < besede.length; i++) {
    var n = sporocilo.toLowerCase().indexOf(besede[i]);
      if (n >= 0) {
        var len = besede[i].length; 
        var len2 = sporocilo.length;
        var cenzura = "";
      
        for (var k = 0; k < len; k++)
          cenzura += "*";
        
        if (n === 0) {
          if ((n+len <= len2-1) && sporocilo[n+len].match(/[a-z]/i)) {
              continue;
          } else {
              sporocilo = sporocilo.replace(sporocilo.substring(n, n+len), cenzura);
          }
        } else {
          if (sporocilo[n-1].match(/[a-z]/i)) {
            if ((n+len <= len2-1) && sporocilo[n+len].match(/[a-z]/i)) {
              continue;
            }
          } else {
              sporocilo = sporocilo.replace(sporocilo.substring(n, n+len), cenzura);
          }
        }
      }
    }
  return sporocilo;

}

function divElementEnostavniTekst(sporocilo) {
  
  var tekst = spremeni(sporocilo);
  tekst = zamenjajSmejkote(tekst);
  
  return $('<div style="font-weight: bold"></div>').html(tekst);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    //var message = preveriVulgarneBesede(sporocilo);
    var message = razdelipoPresledkih(sporocilo);
    klepetApp.posljiSporocilo($("#kanal").text().substring($("#kanal").text().indexOf("@")+2), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(message));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var nickname = "";
  
  $.get('swearWords.txt', function(content) {
    besede = content.split("\n");
  });

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek + " @ Skedenj");
      nickname = rezultat.vzdevek;
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(nickname + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(divElementEnostavniTekst(sporocilo.besedilo));
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
     for (var uporabnik in uporabniki) {
      //uporabniki = uporabniki.substring(1, uporabniki.length);
      if (uporabniki !== '') {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[uporabnik]));
      }
    }
  });

  socket.on('zasebnoSporocilo', function(sporocilo) {
    var message = razdelipoPresledkih(sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(message));
  });
  
  socket.on('kreiranjeKanalaZGeslom', function(sporocilo) {
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});