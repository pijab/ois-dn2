var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      var tekst = besede.join(" ");
      var imeSobe = "";
      var geslo = "";
      var jeParameter1 = true;
      if (tekst[0] === '"') {
         for (var i = 1; i < tekst.length; i++) {
          if (tekst[i] !== '"' && jeParameter1 === true) {
            imeSobe = imeSobe + tekst[i];
          } else {
            jeParameter1 = false;
          }
          if (tekst[i] !== '"' && jeParameter1 === false)
            geslo = geslo + tekst[i];
         }
         geslo = geslo.trim();
         this.socket.emit('kreiranjeKanalaZGeslom', imeSobe, geslo);
      } else {
        this.spremeniKanal(kanal);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      for (var i = 2; i < besede.length; i++) {
        besede[1] = besede[1] + " " + besede[i];
        delete besede[i];
      }
      if (besede[0][0] === '"' && besede[0][besede[0].length-1] === '"') {
        if (besede[1][0] === '"' && besede[1][besede[1].length-1] === '"') {
          var parameter1 = besede[0].substring(1, besede[0].length-1);
          var parameter2 = besede[1].substring(1, besede[1].length-1);
           this.socket.emit('zasebnoSporocilo', parameter1, parameter2);
        } 
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};