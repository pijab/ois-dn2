var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var tabelaKanalov = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    tabelaKanalov['Skedenj'] ='*';
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    posiljanjeZasebnihSporocil(socket);
    obdelajKanalZGeslom(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajPridruzitevUporabnika(socket);
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajPridruzitevUporabnika(socket) {
  socket.on('uporabniki', function() {
    var tabelaUporabnikov = [];
    var tabelaUporabnikovId = io.sockets.clients(trenutniKanal[socket.id]);
    for (var i in tabelaUporabnikovId) {
       tabelaUporabnikov[i] = vzdevkiGledeNaSocket[tabelaUporabnikovId[i].id];
    }
    socket.emit('uporabniki', tabelaUporabnikov);
  });
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function posiljanjeZasebnihSporocil(socket) {
  socket.on('zasebnoSporocilo', function (vzdevek, sporocilo) {
    var posiljatelj = vzdevkiGledeNaSocket[socket.id];
    var idPrejemnika = null;
    for (var i in vzdevkiGledeNaSocket) {
      if (vzdevkiGledeNaSocket[i] === vzdevek) 
        idPrejemnika = i;
    }

    if (idPrejemnika === null) {
      socket.emit('zasebnoSporocilo', "Sporočilo " + sporocilo + " uporabniku z vzdevkom " + vzdevek + " ni bilo mogoče posredovati.");
    } else {
      if (posiljatelj === vzdevek) {
       socket.emit('zasebnoSporocilo', "Sporočilo " + sporocilo + " uporabniku z vzdevkom " + vzdevek + " ni bilo mogoče posredovati.");
      } else {
        io.sockets.socket(idPrejemnika).emit('zasebnoSporocilo', posiljatelj + " (zasebno): " + sporocilo);
        socket.emit('zasebnoSporocilo', "(zasebno za " + vzdevek + "): " + sporocilo);
      } 
    }
  });
}

function obdelajKanalZGeslom(socket) {
  socket.on('kreiranjeKanalaZGeslom', function(kanal, geslo) {
    if (tabelaKanalov[kanal] === "*") {
      socket.emit('kreiranjeKanalaZGeslom', "Izbrani kanal " + kanal + " je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev " + kanal + " ali zahtevajte kreiranje kanala z drugim imenom.");
      return;
    }
    if (tabelaKanalov[kanal] !== undefined) {
      if (tabelaKanalov[kanal] === geslo) {
        socket.leave(trenutniKanal[socket.id]);
        izbrisiSobo(socket.id);
        pridruzitevKanalu(socket, kanal);
      } else {
        socket.emit('kreiranjeKanalaZGeslom', "Pridružitev v kanal " + kanal + " ni bilo uspešno, ker je geslo napačno!");
      }
    } else {
      socket.leave(trenutniKanal[socket.id]);
      izbrisiSobo(socket.id);
      tabelaKanalov[kanal] = geslo;
      pridruzitevKanalu(socket, kanal);
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
  
  if (tabelaKanalov[kanal.novKanal] !== undefined) {
    if (tabelaKanalov[kanal.novKanal] === "*") {
      socket.leave(trenutniKanal[socket.id]);
      izbrisiSobo(socket.id);
      pridruzitevKanalu(socket, kanal.novKanal);
    } else {
      socket.emit('kreiranjeKanalaZGeslom', "Pridružitev v kanal " + kanal.novKanal + " ni bilo uspešno, ker je geslo napačno!");
    }
  } else {
    tabelaKanalov[kanal.novKanal] = "*"; 
    socket.leave(trenutniKanal[socket.id]);
    izbrisiSobo(socket.id);
    pridruzitevKanalu(socket, kanal.novKanal);
  }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}

function izbrisiSobo(idU) {
  if (io.sockets.clients(trenutniKanal[idU]).length === 0)
    delete tabelaKanalov[trenutniKanal[idU]];
}